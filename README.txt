INTRODUCTION
------------

This light-weight module enables opting in of any text field or text area of
nodes, webforms and comments for validation against the pre-defined undesired
regular expressions to automatically deny anonymous submissions when such
strings are detected. The Block Anonymous Strings module is the most efficient
and the least intrusive anti-spam tool without having to rely on third-party
services.

The Block Anonymous Strings module also supports Drupal's native contact forms.

HOW IT WORKS
------------

If an anonymous user tries to post a webform, node or comment with a text field
or a text area value that contains an undesired string, then the submission is
declined giving a preset error message. Admins can opt any of the node, webform
or comment text area or field for validation against the regular expression
strings defined on the modules configuration page.

Similarly, the module protects the name, subject and message fields of site-wide
and users' personal contact forms.

ALTERNATIVE MODULES
-------------------

You might also consider using other such anti-spam modules as, for example:

Block Anonymous Links - https://www.drupal.org/project/blockanonymouslinks
Honeypot - https://www.drupal.org/project/honeypot
AntiSpam - https://www.drupal.org/project/antispam
CAPTCHA - https://www.drupal.org/project/captcha
Spam - https://www.drupal.org/project/spam

which are good alternatives depending on your needs. However, note that compared
to the Block Anonymous Strings module, the listed modules rely on third party
services, are too intrusive, provide only partial protection or have heavy and
large code-base.

INSTALLATION
------------

* Download the recommended version of the module to your website's modules
  directory, go to the Modules page and enable the Block Anonymous Strings.
  Alternatively, just run `drush en block_anonymous_strings` command on CLI.

* Go to the Block Anonymous Strings' settings page and set the error message
  text and the undesired regular expressions.

* Go to any text area's or text field's (of node, webform or comment entities)
  configuration edit page and opt in for anti-spam validation.

  TROUBLESHOOTING
  ---------------

Report any problems found on the module's issues page:
https://www.drupal.org/project/issues/block_anonymous_strings
